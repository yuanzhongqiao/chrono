<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意力</font></font></h1><a id="user-content-attention" class="anchor" aria-label="永久链接：注意" href="#attention"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chrono git 存储库的结构更改如下：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主要开发分支现在称为</font></font><code>main</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（以前</font></font><code>develop</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该</font></font><code>master</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支现已过时，已被删除</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">版本位于名为的分支中</font></font><code>release/*.*</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，并具有以下形式的标签</font></font><code>*.*.*</code></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">计时计划</font></font></h1><a id="user-content-project-chrono" class="anchor" aria-label="永久链接：CHRONO 项目" href="#project-chrono"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://gitlab.com/uwsbel/chrono/commits/main" rel="nofollow"><img src="https://camo.githubusercontent.com/df01d9cd5a177ba2c927122feb158ed5b41b52af9bf8b8b006ef641f82454327/68747470733a2f2f6769746c61622e636f6d2f75777362656c2f6368726f6e6f2f6261646765732f6d61696e2f706970656c696e652e737667" alt="管道状态" data-canonical-src="https://gitlab.com/uwsbel/chrono/badges/main/pipeline.svg" style="max-width: 100%;"></a>
<a href="https://projectchrono.org/license-chrono.txt" rel="nofollow"><img src="https://camo.githubusercontent.com/54bf4f0b0a30d2669c1336bf66fb5112595483bfd54d8ccb420870b703c45e3e/687474703a2f2f7777772e70726f6a6563746368726f6e6f2e6f72672f6173736574732f6c6f676f732f6368726f6e6f2d6273642e737667" alt="BSD 许可证" data-canonical-src="http://www.projectchrono.org/assets/logos/chrono-bsd.svg" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chrono 在宽松的 BSD 许可证下分发，是一个开源多物理场软件包，用于建模和模拟：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由微分代数方程 (DAE) 控制的大型连接刚体系统的动力学</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由偏微分方程 (PDE) 控制的变形体动力学</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用导致微分变分不等式 (DVI) 问题的非光滑接触公式或导致 DAE 的光滑接触公式的颗粒动力学</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其动力学由耦合 DAE 和 PDE 控制的流固相互作用问题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由常微分方程 (ODE) 控制的一阶动态系统</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chrono 提供了成熟稳定的代码库，并不断增加新功能和模块。 Chrono 的核心功能为刚性和柔性多体系统的建模、仿真和可视化提供支持，并通过可选模块提供附加功能。这些模块为其他类别的问题（例如，颗粒动力学和流固相互作用）、专用系统（例如地面车辆）的建模和仿真、协同仿真、运行时可视化、后处理、外部接口提供支持。线性求解器或用于大规模模拟的专用并行计算算法（多核、GPU 和分布式）。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chrono 被学术界、工业界和政府的研究人员用于解决许多不同的科学和工程问题，它为多体动力学、有限元分析、颗粒动力学、流固相互作用、地面车辆仿真和车辆地形相互作用提供了成熟而复杂的支持。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chrono 几乎完全用 C++ 实现，还提供 Python 和 C# API。构建系统基于 CMake。 Chrono 独立于平台，并使用各种编译器在 Linux、Windows 和 MacOS 上进行了积极测试。</font></font></p>
<ul dir="auto">
<li><a href="http://projectchrono.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目网站</font></font></a></li>
<li><a href="https://api.projectchrono.org/development/tutorial_table_of_content_install.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建和安装说明</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></h3><a id="user-content-documentation" class="anchor" aria-label="永久链接：文档" href="#documentation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">C++ API 参考
</font></font><ul dir="auto">
<li><a href="http://api.projectchrono.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主要开发分支</font></font></a></li>
<li><a href="http://api.projectchrono.org/8.0.0/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">版本8.0.0</font></font></a></li>
<li><a href="http://api.projectchrono.org/7.0.0/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7.0.0 版本</font></font></a></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python接口
</font></font><ul dir="auto">
<li><a href="https://api.projectchrono.org/pychrono_introduction.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">皮克罗诺</font></font></a></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">参考手册
</font></font><ul dir="auto">
<li><a href="https://api.projectchrono.org/manual_root.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">核心模块</font></font></a></li>
<li><a href="https://api.projectchrono.org/manual_vehicle.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chrono::车辆模块</font></font></a></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></h3><a id="user-content-support" class="anchor" aria-label="永久链接： 支持" href="#support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://groups.google.com/g/projectchrono" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google 网上论坛用户论坛</font></font></a></li>
</ul>
</article></div>
